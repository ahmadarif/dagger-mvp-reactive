package com.ahmadarif.daggermvp;

import android.app.Application;

import com.ahmadarif.daggermvp.di.component.DaggerAppComponent;
import com.ahmadarif.daggermvp.di.component.AppComponent;
import com.ahmadarif.daggermvp.di.module.AppModule;
import com.ahmadarif.daggermvp.di.module.NetModule;

/**
 * Created by ARIF on 04-Jun-17.
 */
public class App extends Application {

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule("https://alamat.daily-event.com/api/"))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
