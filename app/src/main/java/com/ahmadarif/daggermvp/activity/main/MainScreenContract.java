package com.ahmadarif.daggermvp.activity.main;

import com.ahmadarif.daggermvp.di.Province;

/**
 * Created by ARIF on 04-Jun-17.
 */

public interface MainScreenContract {

    interface View {
        void showPosts(Province provinces);

        void showError(String message);

        void showComplete();
    }

    interface Presenter {
        void loadPost();
    }

}
