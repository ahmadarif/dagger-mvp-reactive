package com.ahmadarif.daggermvp.activity.main;

import com.ahmadarif.daggermvp.di.scope.MainActivityScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ARIF on 04-Jun-17.
 */

@Module
public class MainScreenModule {
    private final MainScreenContract.View mView;


    public MainScreenModule(MainScreenContract.View mView) {
        this.mView = mView;
    }

    @Provides
    @MainActivityScope
    MainScreenContract.View providesMainScreenContractView() {
        return mView;
    }
}