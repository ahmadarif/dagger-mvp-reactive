package com.ahmadarif.daggermvp.activity.main;

import com.ahmadarif.daggermvp.di.component.AppComponent;
import com.ahmadarif.daggermvp.di.scope.MainActivityScope;

import dagger.Component;

/**
 * Created by ARIF on 04-Jun-17.
 */

@MainActivityScope
@Component(dependencies = AppComponent.class, modules = MainScreenModule.class)
public interface MainScreenComponent {
    void inject(MainActivity activity);
}