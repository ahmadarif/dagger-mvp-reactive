package com.ahmadarif.daggermvp.di.component;

import com.ahmadarif.daggermvp.di.module.AppModule;
import com.ahmadarif.daggermvp.di.module.NetModule;
import com.ahmadarif.daggermvp.di.scope.AppScope;

import dagger.Component;
import retrofit2.Retrofit;

/**
 * Created by ARIF on 04-Jun-17.
 */
@AppScope
@Component(modules = {AppModule.class, NetModule.class})
public interface AppComponent {
    // downstream components need these exposed with the return type
    // nama method tidak penting
    Retrofit retrofit();
}