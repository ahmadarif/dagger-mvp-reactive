package com.ahmadarif.daggermvp.di.module;

import android.app.Application;

import com.ahmadarif.daggermvp.di.scope.AppScope;

import dagger.Module;
import dagger.Provides;

/**
 * Created by ARIF on 04-Jun-17.
 */
@Module
public class AppModule {

    Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @AppScope
    @Provides
    Application provideApplication() {
        return application;
    }

}
