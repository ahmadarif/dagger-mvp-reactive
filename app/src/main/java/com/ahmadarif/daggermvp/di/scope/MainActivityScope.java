package com.ahmadarif.daggermvp.di.scope;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by ARIF on 04-Jun-17.
 */

@Documented
@Scope
@Retention(RetentionPolicy.RUNTIME)
public @interface MainActivityScope {
}