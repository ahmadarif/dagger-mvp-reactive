package com.ahmadarif.daggermvp.di;

import java.util.List;

/**
 * Created by ARIF on 04-Jun-17.
 */

public class Province {

    private final Integer current_page;
    private final Integer last_page;
    private final List<Data> data;

    public Province(Integer current_page, Integer last_page, List<Data> data) {
        this.current_page = current_page;
        this.last_page = last_page;
        this.data= data;
    }

    public Integer getCurrent_page() {
        return current_page;
    }

    public Integer getLast_page() {
        return last_page;
    }

    public List<Data> getData() {
        return data;
    }

}