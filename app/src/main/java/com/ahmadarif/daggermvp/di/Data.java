package com.ahmadarif.daggermvp.di;

/**
 * Created by ARIF on 05-Jun-17.
 */

public class Data {

    private final String id;
    private final String name;

    public Data(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

}